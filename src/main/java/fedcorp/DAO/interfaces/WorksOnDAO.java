package fedcorp.DAO.interfaces;

import fedcorp.model.PK_WorksOn;
import fedcorp.model.WorksOnEntity;

public interface WorksOnDAO extends GeneralDAO<WorksOnEntity, PK_WorksOn> {
}
