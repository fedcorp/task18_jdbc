package fedcorp.DAO.interfaces;

import fedcorp.model.DepartmentEntity;

public interface DepartmentDAO extends GeneralDAO<DepartmentEntity, String> {
}


