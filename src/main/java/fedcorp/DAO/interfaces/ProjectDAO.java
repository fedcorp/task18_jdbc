package fedcorp.DAO.interfaces;

import fedcorp.model.ProjectEntity;

public interface ProjectDAO extends GeneralDAO<ProjectEntity, String> {
}
